<?php
require_once("bd.php");
$link = SDb::DBCon();
$ip = "62.221.70.78";

if(isset($_POST['message'])&& $_POST['message'] != "" && isset($_POST['authorname'])&&$_POST['authorname'] != "") {
    $query = SDb::bd_insert($link, $_POST['authorname'], $_POST['message']);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My comments!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<h3 class="text-center">All comments</h3>
					<?php $query= SDb::bd_read($link); ?> <!-- load comments-->
				</div>
				<div class="col-md-6">
					<h3 class="text-center">Add comment</h3>
					<div class="row">
						<div class="col-md-2">
						</div>
						<div class="col-md-8">
                            <?php
                                if(Access::checkCountry($ip) != "MD"){ //$_SERVER['REMOTE_ADDR'];
                                    echo "
                                        <div class=\"alert alert-dismissable alert-danger\">
								            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
								            <h4>Alert!</h4><strong>Warning!</strong> Add comments only for users from <mark>Moldova</mark>.
							            </div>
                                        ";
                                }
                            ?>

							<form name="addcomment" method="POST" class="form-horizontal" role="form">
								<div class="form-group">

									<label class="col-sm-2 control-label">Your name</label>
									<div class="col-sm-10">
										<input name="authorname" type="text" class="form-control" <?php if(Access::checkCountry($ip) != "MD") echo "readonly"; ?>>
									</div>
								</div>
								<div class="form-group">
									 
									<label class="col-sm-2 control-label">Message</label>
									<div class="col-sm-10">
										<textarea name="message" class="form-control" rows="3" <?php if(Access::checkCountry($ip) != "MD") echo "readonly"; ?>></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-default" <?php if(Access::checkCountry($ip) != "MD") echo "disabled=\"disabled\""; ?>>Add a comment</button>
									</div>
								</div>
							</form>
						</div>
						<div class="col-md-2">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>